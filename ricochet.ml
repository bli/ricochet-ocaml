open Base


let skip_comments = List.filter ~f:(fun (line: string) -> not (Char.equal '#' line.[0]))

(* Read a string containing space-separated ints into a list of ints *)
let parse_int_line line = List.map (String.split line ~on:' ') ~f:Int.of_string

module Direction = struct
  type t = E | S | W | N
  let of_string = function
    | "E" -> E
    | "S" -> S
    | "W" -> W
    | "N" -> N
    | _ -> failwith "Direction can only be either E, S, W or N."
  let to_string = function
    | E -> "E"
    | S -> "S"
    | W -> "W"
    | N -> "N"
end

(* TODO: share interface between Robot and Goal? *)
module Robot = struct
  type t =
  {
    id: int;
    mutable row: int;
    mutable col: int;
  }
  let make_robot robot_line =
    match parse_int_line robot_line with
      | [id; row; col] -> {id=id; row=row; col=col;}
      | _ -> failwith (Printf.sprintf
          "Wrong line structure:\n%s\nThree space-separated columns expected."
          robot_line)
    (* this pattern-matching is not exhaustive
    let [id; row; col] = parse_int_line robot_line in
    {id=id; row=row; col=col;}
    *)
  let move (robot: t) (dir: Direction.t) =
    match dir with
    | E -> robot.col <- robot.col + 1;
    | S -> robot.row <- robot.row + 1;
    | W -> robot.col <- robot.col - 1;
    | N -> robot.row <- robot.row - 1;

end

module Goal = struct
  type t =
  {
      id: int;
      row: int;
      col: int;
  }
  let make_goal goal_line =
    match parse_int_line goal_line with
      | [id; row; col] -> {id=id; row=row; col=col;}
      | _ -> failwith (Printf.sprintf
          "Wrong line structure:\n%s\nThree space-separated columns expected."
          goal_line)
    (* this pattern-matching is not exhaustive
    let [id; row; col] = parse_int_line goal_line in
    {id=id; row=row; col=col;}
    *)
end

module Cell = struct
  type t =
  {
    row: int;
    col: int;
    mutable east: bool;
    mutable south: bool;
    mutable west: bool;
    mutable north: bool;
    mutable robot: Robot.t option;
    mutable goal: Goal.t option;
  }
  let make_row row_num wt =
    Array.init wt ~f:(fun col_num -> {
      row=row_num; col=col_num;
      east=false; south=false; west=false; north=false;
      robot = None;
      goal = None;})
  let make_2D wt ht = Array.init ht ~f:(fun row_num -> make_row row_num wt)
end

class board infile_path =
  (* let skip_comments = List.filter ~f:(fun line -> line.[0] <> '#') in *)
  let init_lines = Stdio.In_channel.with_file infile_path ~f:(fun file ->
    Array.of_list (skip_comments (Stdio.In_channel.input_lines file))) in
  (* this pattern-matching is not exhaustive
  let [wt; ht; nb_walls; nb_robots; nb_goals] = parse_int_line init_lines.(0) in
  *)
  (* Temporarilly desactivate warning 8 *)
  let[@warning "-8"] [wt; ht; nb_walls; nb_robots; nb_goals] = parse_int_line
      init_lines.(0)[@warning "+8"] in
  let wall_lines = Array.sub init_lines ~pos:1 ~len:nb_walls in
  let robot_lines = Array.sub init_lines ~pos:(1 + nb_walls) ~len:nb_robots in
  let goal_lines =  Array.sub init_lines ~pos:(1 + nb_walls + nb_robots) ~len:nb_goals in
  object(self)
    val cells: Cell.t Array.t Array.t = Cell.make_2D wt ht
    val robots: Robot.t Array.t = Array.init nb_robots ~f:(fun i ->
      Robot.make_robot(robot_lines.(i)))
    val goals: Goal.t Array.t = Array.init nb_goals ~f:(fun i ->
      Goal.make_goal(goal_lines.(i)))
    method get_cells = cells
    method get_cell (x: int) (y: int) = cells.(x).(y)
    method get_robot (robot_id: int) = robots.(robot_id)
    method move (robot_id: int) (dir: Direction.t) =
      Robot.move robots.(robot_id) dir;
    (* initializer self#place_walls init_file; *)
  end
;;

let b = new board "/pasteur/homes/bli/Documents/Learning/Ricochet_robot/ricochet/sample/example_1.in";;
